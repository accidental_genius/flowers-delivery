from authif import Auth
from signup import SignUp
from mainwindow import FlowersPage

from PyQt6.QtWidgets import QApplication
import sys

app = QApplication(sys.argv)

auth = Auth()
signup = SignUp()
flowers_page = FlowersPage()
auth.next(lambda: signup.show())
auth.next1(lambda: flowers_page.show())
auth.show()

sys.exit(app.exec())