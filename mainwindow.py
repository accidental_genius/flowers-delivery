from PyQt6.QtWidgets import QWidget, QDialog, QVBoxLayout, QLabel, QFormLayout, QLineEdit, QPushButton
from PyQt6 import uic
from sqlalchemy import and_

from database import engine, Bouquets, Order
from sqlalchemy.orm import Session


class FlowersPage(QWidget):
    def __init__(self):
        super(FlowersPage, self).__init__()
        uic.loadUi('./ui/mainpage.ui', self)
        self.pushButton.clicked.connect(self.__roses_order_clicked)
        self.pushButton_2.clicked.connect(self.__hriz_order_clicked)
        self.pushButton_3.clicked.connect(self.__gerb_order_clicked)
        self.pushButton_4.clicked.connect(self.__tulips_order_clicked)
        self.roses_color = ['Красные', 'Белые', 'Розовые']
        self.colorcomboBox.addItems(self.roses_color)
        self.hriz_color = ['Красные', 'Белые', 'Розовые']
        self.colorcomboBox_2.addItems(self.hriz_color)
        self.gerb_color = ['Красные', 'Белые', 'Розовые']
        self.colorcomboBox_3.addItems(self.gerb_color)
        self.tulips_color = ['Красные', 'Белые', 'Розовые']
        self.colorcomboBox_4.addItems(self.tulips_color)

    def __roses_order_clicked(self):
        if self.smallradioBtn.isChecked():
            quantity = self.quantitySpinBox.value()
            print(type(quantity))
            print(quantity)
            price = self.smallpriceLabel.text().split(' ')
            price = int(price[0])
            print(type(price))
            print(price)
            session = Session(bind=engine)
            b = session.query(Bouquets.id).filter(and_(Bouquets.size == self.smallradioBtn.text(),
                                                       Bouquets.price == self.smallpriceLabel.text(),
                                                       Bouquets.name == self.label_3.text(),
                                                       Bouquets.color == self.colorcomboBox.currentText())).first()
            print(b)
            # bouq_id = []
            # for row in b:
            #     print(row)
            #     bouq_id = row
            # print(bouq_id)

            # order = Order(
            #     customers_id=1,
            #     bouquets_id=b.id,
            #     quantity=int(quantity),
            #     total_price=int(quantity) * price)
            #
            # session.add(order)
            # session.commit()

    def __hriz_order_clicked(self):
        pass

    def __gerb_order_clicked(self):
        pass

    def __tulips_order_clicked(self):
        pass

# class OrderDialog(QDialog):
#     def __init__(self):
#         super(OrderDialog, self).__init__()
#         uic.loadUi('./ui/orderdialog.ui', self)
#         self.pushButton.clicked.connect(self.okbtn_clicked)
#
#         #self.dialog.show()
#
#
#
#     def okbtn_clicked(self, ):
#         uic.loadUi('./ui/mainpage.ui', self)
