from PyQt6.QtWidgets import QWidget, QDialog, QVBoxLayout, QLabel
from PyQt6 import uic, QtGui
from database import engine, Customers, Order
from sqlalchemy.orm import Session
import socket
import json

class Auth(QWidget):
    def __init__(self):
        super(Auth, self).__init__()
        uic.loadUi('./ui/loginform.ui', self)
        self.pushBtnLogin.clicked.connect(self.__login_clicked__)
        self.pushBtnSignup.clicked.connect(self.__sign_up_clicked)
        self.__fun = None
        self.__funct = None
        self.setFixedSize(730,500)
        self.setWindowTitle('Вход')

    def next(self, funct):
        self.__funct = funct

    def next1(self, fun):
        self.__fun = fun

    def __login_clicked__(self):
        if self.lineEditPassword.text() == '' or self.lineEditEmail.text() == '':
            dialog = QDialog(parent=self)
            layout = QVBoxLayout()
            label = QLabel('Пожалуйста, заполните все поля!')
            layout.addWidget(label)
            dialog.setLayout(layout)
            dialog.show()
        else:
            HOST = '127.0.0.1'
            PORT = 63788
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.connect((HOST, PORT))
                info_to_valid = {
                    'email': self.lineEditEmail.text(),
                    'password': self.lineEditPassword.text()
                }
                print(type(info_to_valid))
                login_data = json.dumps(info_to_valid, separators=(',', ':'))
                print(login_data)
                s.send(bytes(f'{login_data}', encoding='UTF-8'))
                ans = s.recv(1024).decode("UTF-8")
                if ans == "+OK":
                    print('ok')
                    self.close()
                    self.__fun()
                else:
                    print('неправильный логин или пароль')
                    self.lineEditPassword.setText('')
                    self.lineEditEmail.setText('')



    def __sign_up_clicked(self):
        self.__funct()

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        return super().closeEvent(a0)


class SignUp(QWidget):
    def __init__(self, parent=None):
        super(SignUp, self).__init__(parent)
        uic.loadUi('./ui/registration.ui', self)
        self.lineEditName.setFocus()
        self.pushBtnRegister.clicked.connect(self.__registerBtn_clicked)

    def __adding_info(self):
        if self.lineEditName.text() and self.lineEditSurname.text() and self.lineEditEmail.text() and self.lineEditPassword.text() and self.lineEditPhoneNum.text() and self.lineEditAddress.text() != '':
            try:
                customer = Customers(
                    name=self.lineEditName.text(),
                    surname=self.lineEditSurname.text(),
                    email=self.lineEditEmail.text(),
                    password=self.lineEditPassword.text(),
                    phone_number=self.lineEditPhoneNum.text(),
                    address=self.lineEditAddress.text())
                session = Session(bind=engine)
                session.add(customer)
                session.commit()


                dialog = QDialog(parent=self)
                layout = QVBoxLayout()
                label = QLabel('Регистрация прошла успешно!')
                layout.addWidget(label)
                dialog.setLayout(layout)
                dialog.show()
                self.close()
            except Exception as e:
                print(e)
                dialog = QDialog(parent=self)
                layout = QVBoxLayout()
                label = QLabel('Что-то пошло не так, попробуйте еще раз!')
                layout.addWidget(label)
                dialog.setLayout(layout)
                dialog.show()
        else:
            dialog = QDialog(parent=self)
            layout = QVBoxLayout()
            label = QLabel('Пожалуйста, заполните все поля!')
            layout.addWidget(label)
            dialog.setLayout(layout)
            dialog.show()


    def __registerBtn_clicked(self):
        self.__adding_info()
