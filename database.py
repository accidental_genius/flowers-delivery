import mysql.connector
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, MetaData, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
import socket
import json
from datetime import datetime

try:
    mydb = mysql.connector.connect(
        host='localhost',
        user='root',
        password='root'
    )

    mycursor = mydb.cursor()
    mycursor.execute('CREATE DATABASE flowers_delivery1')
except:
    pass

Base = declarative_base()

engine = create_engine('mysql+pymysql://root:root@localhost/flowers_delivery1')


class Bouquets(Base):
    __tablename__ = 'bouquets'
    id = Column(Integer(), primary_key=True)
    size = Column(String(25), nullable=False)
    price = Column(String(20), nullable=False)
    name = Column(String(35), nullable=False)
    color = Column(String(20), nullable=False)


class Order(Base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True)
    customers_id = Column(Integer(), ForeignKey('customers.id'))
    bouquets_id = Column(Integer(), ForeignKey('bouquets.id'))
    quantity = Column(Integer(), nullable=False)
    total_price = Column(Integer(), nullable=False)
    date_of_order = Column(DateTime(), default=datetime.now(), nullable=False)


class Customers(Base):
    __tablename__ = 'customers'
    id = Column(Integer(), primary_key=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(35), nullable=False)
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(20), nullable=False, unique=True)
    phone_number = Column(String(20), nullable=False, unique=True)
    address = Column(String(200), nullable=False)


Base.metadata.create_all(engine)

HOST = '127.0.0.1'
PORT = 63788


def server_to_check():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        while True:
            print('Server is running, please press Ctrl + C to stop')
            conn, addr = s.accept()
            print('connected: ', addr)
            data = conn.recv(1024).decode("UTF-8")  # получает от клиента отправленные данные
            info_from_user = json.loads(data)  # записывает полученные данные в переменную
            session = Session(bind=engine)
            result = session.query(Customers.email, Customers.password).filter(
                Customers.email == info_from_user['email'],
                Customers.password == info_from_user['password'])
            data = []
            for row in result:
                if row[0] == info_from_user['email'] and row[1] == info_from_user['password']:
                    data = row
            if len(data) == 0:
                conn.send(bytes("+NOK", encoding="UTF-8"))
            else:
                conn.send(bytes(f"+OK", encoding="UTF-8"))


name = ['Розы', 'Хризантемы', 'Герберы', 'Тюльпаны']
price = ['150 TMT', '320 TMT', '500 TMT']
size = ['Уменьшенный', 'Стандарт', 'Делюкс']
color = ['Красный', 'Белый', 'Розовый']


def bouquets_info_add():
    for n in name:
        for c in color:
            for (p, s) in zip(price, size):
                print(n, p, s, c)
                bouquets = Bouquets(
                    name=n,
                    price=p,
                    size=s,
                    color=c)
                session = Session(bind=engine)
                session.add(bouquets)
                session.commit()


#bouquets_info_add()

if __name__ == '__main__':
    server_to_check()
